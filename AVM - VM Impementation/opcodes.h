#ifndef __HEADER_OPCODES
#define __HEDAER_OPCODES

#define OP_ICONST_0			0x03
#define OP_ICONST_1			0x04
#define OP_ICONST_2			0x05
#define OP_ICONST_3			0x06
#define OP_ICONST_4			0x07
#define OP_ICONST_5			0x08

#define OP_INVOKESPECIAL	0xB7
#define OP_INVOKESTATIC		0xB8

#define OP_GOTO				0xA7

#define OP_BIPUSH			0x10
#define OP_SIPUSH			0x11

/// Pop integer from operand stack and move to local variable table (LVT <- OPS) ///
#define OP_ISTORE_0			0x3B
#define OP_ISTORE_1			0x3C

#define OP_ASTORE_0			0x4B

/// Move integer from local variable at index 0 and push on operand stack (LVT -> OPS) ///
#define OP_ILOAD_0			0x1A

#define OP_IAND				0x7E

#define OP_ALOAD_0			0x2A

#define OP_RETURN			0xB1
#define OP_IRETURN			0xAC

#define OP_IADD				0x60

#define OP_PUTSTATIC		0xB3
#define OP_GETSTATIC		0xB2

#define OP_PUTFIELD			0xB5
#define OP_GETFIELD			0xB4

#define OP_NEW				0xBB

#define OP_DUP				0x59

#define OP_IFEQ				0x99

#define OP_ISHR				0x7A

#define OP_I2B				0x91

#define OP_I2S				0x93

#define OP_NOP				0x00
#define OP_METHOD_END		0xFF // (impdep2 - reserved for implementation-dependent operations within debuggers; should not appear in any class file)

#endif