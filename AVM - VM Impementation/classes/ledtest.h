#ifndef __HEADER__CLASS_LEDTEST
#define __HEADER__CLASS_LEDTEST

#include <avr/pgmspace.h>

u1 ledtest_class_signatures[4] PROGMEM = 
{
	0xDF, 0x8A,
	0x00, 0x00,
};

#define LEDTEST_CLASS_METHODS_SIZE		124
u1 ledtest_class_methods[LEDTEST_CLASS_METHODS_SIZE] PROGMEM = 
{
	0x00, 0x01, 0xB2, 0xD1, 0x00, 0x01, 0x00, 0x00, 0x00, 0x72, 0x10, 0x14, 0x11, 0x00, 0xFF, 
	0xB8, 0x00, 0x10, 0x10, 0x15, 0x03, 0xB8, 0x00, 0x10, 0x10, 0x15, 0x03, 0xB8, 0x00, 0x16, 
	0x11, 0x02, 0xBC, 0xB8, 0x00, 0x19, 0x10, 0x15, 0x04, 0xB8, 0x00, 0x16, 0x11, 0x02, 0xBC, 
	0xB8, 0x00, 0x19, 0x10, 0x15, 0x05, 0xB8, 0x00, 0x16, 0x11, 0x02, 0xBC, 0xB8, 0x00, 0x19, 
	0x10, 0x15, 0x06, 0xB8, 0x00, 0x16, 0x11, 0x02, 0xBC, 0xB8, 0x00, 0x19, 0x10, 0x15, 0x07, 
	0xB8, 0x00, 0x16, 0x11, 0x02, 0xBC, 0xB8, 0x00, 0x19, 0x10, 0x15, 0x08, 0xB8, 0x00, 0x16, 
	0x11, 0x02, 0xBC, 0xB8, 0x00, 0x19, 0x10, 0x15, 0x10, 0x06, 0xB8, 0x00, 0x16, 0x11, 0x02, 
	0xBC, 0xB8, 0x00, 0x19, 0x10, 0x15, 0x10, 0x07, 0xB8, 0x00, 0x16, 0x10, 0x07, 0xB8, 0x00, 
	0x1F, 0xA7, 0xFF, 0x9F 
};

#define LEDTEST_CLASS_FIELDS_SIZE		2
u1 ledtest_class_fields[LEDTEST_CLASS_FIELDS_SIZE] PROGMEM = 
{
	0x00, 0x00 
};

#define LEDTEST_CLASS_CONST_POOLS_SIZE		55
u1 ledtest_class_const_pools[LEDTEST_CLASS_CONST_POOLS_SIZE] PROGMEM = 
{
	0x00, 0x26, 0x87, 0x81, 0x87, 0x81, 0x81, 0x81, 0x81, 0x8A, 0x8C, 0x81, 0x81, 0x81, 0x81, 
	0x81, 0x81, 0x0A, 0x10, 0x00, 0x50, 0x04, 0x87, 0x81, 0x8C, 0x81, 0x81, 0x0A, 0x10, 0x00, 
	0x50, 0x06, 0x8C, 0x81, 0x0A, 0x20, 0x00, 0x60, 0x02, 0x87, 0x81, 0x8C, 0x81, 0x81, 0x0A, 
	0x20, 0x00, 0x60, 0x01, 0x8C, 0x81, 0x81, 0x81, 0x81, 0x81 
};

#define LEDTEST_CLASS_INTERFACES_SIZE		2
u1 ledtest_class_interfaces[LEDTEST_CLASS_INTERFACES_SIZE] PROGMEM = 
{
	0x00, 0x00 
};


#endif
