#ifndef __HEADER__CLASSINFO
#define __HEADER__CLASSINFO

/// ============= CLASS FILES BYTECODE ================ ///

#include "ledtest.h"

/// =================================================== ///

#define TOTAL_CLASSES		1

/// SIGNATURES ///

u1* ClassesSignatures[TOTAL_CLASSES] = 
{
	ledtest_class_signatures,
};

/// METHODS ///

u1* ClassesMethods[TOTAL_CLASSES] = 
{
	ledtest_class_methods,
};

int ClassesMethodsSizes[TOTAL_CLASSES] = 
{
	LEDTEST_CLASS_METHODS_SIZE,
};

/// FIELDS ///

u1* ClassesFields[TOTAL_CLASSES] = 
{
	ledtest_class_fields,
};

int ClassesFieldsSizes[TOTAL_CLASSES] = 
{
	LEDTEST_CLASS_FIELDS_SIZE,
};

/// CONST POOLS ///

u1* ClassesConstPools[TOTAL_CLASSES] = 
{
	ledtest_class_const_pools,
};

/// INTERFACES ///

u1* ClassesInterfaces[TOTAL_CLASSES] = 
{
	ledtest_class_interfaces,
};

#endif
