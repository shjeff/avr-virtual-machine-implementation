#ifndef __HEADER_DEFINES
#define __HEADER_DEFINES

/* VM constants and defines */
#define mov_ptr(p, o)				((p)+=(o))

/// Pobieranie z tablicy danych WORD (u2) lub DWORD (u4)
#define	be_get_u2(p)				(u2)((((*((p)))& 0xFF)<<8)|((*((p)+1))& 0xFF))
#define be_get_u4(p)				(u4)((((u4)((*((p)))& 0xFF)) <<24)|(((u4)((*((p)+1))& 0xFF))<<16)|(((*((p)+2))&0xFF)<<8)|((*((p)+3))&0xFF))

/// Tak samo jak wy�ej pobiera z tablicy bajty i pakuje do WORD lub DWORD, ale z dodatkowym przesuni�ciem wska�nika
#define	be_get_u2_m(_v, _p) \
		(_v) = be_get_u2(_p);	\
		mov_ptr(_p, 2)

#define be_get_u4_m(_v, _p)	\
		(_v) = be_get_u4(_p);	\
		mov_ptr(_p, 4)

/// Tak samo jak be_get_u2, ale zamiast czyta� bezpo�rednio z tablicy czyta z pgm_read_byte
/// pgm_read_byte, pgm_read_word, pgm_read_dword pobieraj� liczbe w formacie little endian (odwrotnym)
#define be_pgm_read_u4(addr)		le32_to_be(pgm_read_dword(addr))
#define be_pgm_read_u2(addr)		le16_to_be(pgm_read_word(addr))

//#define be_pgm_read_u2(p)			(u2)((((pgm_read_byte((p)))& 0xFF)<<8)|((pgm_read_byte((p)+1))& 0xFF))
//#define be_pgm_read_u4(p)			(u4)((((u4)((pgm_read_byte((p)))& 0xFF))<<24)|(((u4)((pgm_read_byte((p)+1))& 0xFF))<<16)|(((u4)((pgm_read_byte((p)+2))& 0xFF))<<8)|((u4)((pgm_read_byte((p)+3))& 0xFF)))

/// Task samo jak be_pgm_read_u2, ale z dodatkowym przesuni�ciem
#define be_pgm_read_u2_m(_v, _p) \
		(_v) = be_pgm_read_u2(_p);	\
		mov_ptr(_p, 2);

#define be_pgm_read_u4_m(_v, _p) \
		(_v) = be_pgm_read_u4(_p);	\
		mov_ptr(_p, 4);

/// Wpisywanie do tablicy danych WORD lub DWORD
#define be_u2_get_bytes(u, p) \
	*((p))		= (((u) >>  8) & 0xFF); \
	*((p) + 1)	= ((u)         & 0xFF);

#define be_u4_get_bytes(u, p) \
	*((p))		= (((u) >> 24) & 0xFF); \
	*((p) + 1)	= (((u) >> 16) & 0xFF); \
	*((p) + 2)	= (((u) >>  8) & 0xFF); \
	*((p) + 3)	= ((u)         & 0xFF);

/// Pobieranie pojedynczych bajt�w z danej WORD lub DWORD
#define be_u2_get_st_byte(u)		(u1)((((u))>>8)& 0xFF)
#define be_u2_get_nd_byte(u)		(u1)(((u))& 0xFF)

#define be_u4_get_st_byte(u)		(u1)((((u))>>24)& 0xFF)
#define be_u4_get_nd_byte(u)		(u1)((((u))>>16)& 0xFF)
#define be_u4_get_rd_byte(u)		(u1)((((u))>>8)& 0xFF)
#define be_u4_get_th_byte(u)		(u1)(((u))& 0xFF)

/* Const pool tag type */
#define CONST_POOL_ID_Utf8					1
#define CONST_POOL_ID_Integer				3
#define CONST_POOL_ID_Float					4
#define CONST_POOL_ID_Long					5
#define CONST_POOL_ID_Double				6
#define CONST_POOL_ID_Class					7
#define CONST_POOL_ID_String				8

#define CONST_POOL_ID_Fieldref				9
#define CONST_POOL_ID_Methodref				10
#define CONST_POOL_ID_InterfaceMethodref	11
#define CONST_POOL_ID_NameAndType			12

#define CONST_POOL_ID_SkipPoolFlag			128


#define GET_PTR_OFFSET(b, a, type)			(((u2)(b-a))/sizeof(type))


// 0xENXX XXXX XXXX XXXX, E - entry point, N - native
#define METHOD_SIGNATURE_IS_ENTRY_POINT		(1uL << 15)
#define METHOD_SIGNATURE_IS_NATIVE			(1uL << 14)

// 0xIICX XXXX XXXX XXXX, II (11) - init, C - context
#define METHOD_SIGNATURE_IS_INIT			(3uL << 14)
#define METHOD_SIGNATURE_GET_CONTEXT(m)		(((m) >> 13) & 1uL)
#define  METHOD_SIGNATURE_STATIC_INIT		(1uL)
#define  METHOD_SIGNATURE_OBJECT_INIT		(0uL)

#define FIELD_SIGNATURE_IS_STATIC			(1uL << 15)

#define RESULT_SUCCESS(x)								!!!(x)
#define RESULT_ERROR(x)									!!(x)

#define PGM_ADDR_FIRST_BYTE_PTR(pgm)		&(pgm)[0]

#define __infinity_loop		while(1)

#endif