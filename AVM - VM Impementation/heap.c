int Heap_InitClassesStaticContext(void)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		g_HeapClasses[i] = malloc(sizeof(heap_class_t));

		// Ustaw sygnaturę klasy
		pgm_address_ptr pgm_addr = &ClassesSignatures[i][0];
		g_HeapClasses[i]->m_class_signature = be_pgm_read_u2(pgm_addr);

		// Inicjalizuj konteksty klasy na stercie: statyczny i instancji
		g_HeapClasses[i]->m_object_context = malloc(sizeof(heap_object_context_t));
		g_HeapClasses[i]->m_static_context = malloc(sizeof(heap_static_context_t));

		g_HeapClasses[i]->m_static_context->m_static_fields_count = 0;

		pgm_addr = &ClassesFields[i][0];
		
		u2 field_count = 0;
		be_pgm_read_u2_m(field_count, pgm_addr); 

		for (int j = 0; j < field_count; j++) {
			u2 field_signature = 0;
			be_pgm_read_u2_m(field_signature, pgm_addr);

			if (field_signature & FIELD_SIGNATURE_IS_STATIC)
				g_HeapClasses[i]->m_static_context->m_static_fields_count++;

			type_t field_type = 0;
			be_pgm_read_u4_m(field_type, pgm_addr);
		}

		// Inicjalizacja statycznego kontekstu
		g_HeapClasses[i]->m_static_context->m_static_fields = malloc(sizeof(heap_variable_t) * g_HeapClasses[i]->m_static_context->m_static_fields_count);
		g_HeapClasses[i]->m_non_static_fields_count = field_count - g_HeapClasses[i]->m_static_context->m_static_fields_count;

		pgm_addr = &ClassesFields[i][0];
		mov_ptr(pgm_addr, 2);
		
		int staticFieldSet = 0;
		for (int j = 0; j < field_count; j++) {
			u2 field_signature = 0;
			be_pgm_read_u2_m(field_signature, pgm_addr);

			type_t field_type = 0;
			be_pgm_read_u4_m(field_type, pgm_addr);

			if (field_signature & FIELD_SIGNATURE_IS_STATIC)
			{
				g_HeapClasses[i]->m_static_context->m_static_fields[staticFieldSet].m_signature = field_signature;
				g_HeapClasses[i]->m_static_context->m_static_fields[staticFieldSet].m_type = field_type;
				g_HeapClasses[i]->m_static_context->m_static_fields[staticFieldSet].m_field_value.m_value.v_common = 0;
				staticFieldSet++;
			}
		}

		g_HeapClasses[i]->m_object_context->m_class_instances_count = 0;
	}
	return 0;
}

variable_t* Heap_GetStaticFieldAddress(signature_t class_signature, signature_t field_signature)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		if (g_HeapClasses[i]->m_class_signature != class_signature || !g_HeapClasses[i])
			continue;

		for (int j = 0; j < g_HeapClasses[i]->m_static_context->m_static_fields_count; j++)
		{
			if (g_HeapClasses[i]->m_static_context->m_static_fields[j].m_signature != field_signature)
				continue;

			return &g_HeapClasses[i]->m_static_context->m_static_fields[j].m_field_value;
		}
	}
	return 0;
}

int Heap_SetStaticFieldValue(signature_t class_signature, signature_t field_signature, variable_t value)
{
	variable_t* set_field_addr = Heap_GetStaticFieldAddress(class_signature, field_signature);
	if (set_field_addr)
		*set_field_addr = value;
	return 0;
}

variable_t Heap_GetStaticFieldValue(signature_t class_signature, signature_t field_signature)
{
	return *Heap_GetStaticFieldAddress(class_signature, field_signature);
}

heap_class_instance_t* Heap_CreateClassInstance(signature_t class_signature)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		if (g_HeapClasses[i]->m_class_signature != class_signature)
			continue;

		void* allocated_heap_instance_mem = 0;

		if (g_HeapClasses[i]->m_object_context->m_class_instances_count) {
			allocated_heap_instance_mem = realloc(g_HeapClasses[i]->m_object_context->m_class_instances, sizeof(heap_class_instance_t) * (g_HeapClasses[i]->m_object_context->m_class_instances_count + 1));
		} else {
			allocated_heap_instance_mem = malloc(sizeof(heap_class_instance_t));
		}
		if (!allocated_heap_instance_mem)
			return 0;
		g_HeapClasses[i]->m_object_context->m_class_instances = allocated_heap_instance_mem;
		g_HeapClasses[i]->m_object_context->m_class_instances[g_HeapClasses[i]->m_object_context->m_class_instances_count].m_instance_fields_count = g_HeapClasses[i]->m_non_static_fields_count;
		return &g_HeapClasses[i]->m_object_context->m_class_instances[g_HeapClasses[i]->m_object_context->m_class_instances_count++];
	}
	return 0;
}

int Heap_DestroyClassInstance(signature_t class_signature, heap_class_instance_t* heap_class_instance)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		if (g_HeapClasses[i]->m_class_signature != class_signature)
			continue;

		if (g_HeapClasses[i]->m_object_context->m_class_instances_count > 1) {
			g_HeapClasses[i]->m_object_context->m_class_instances = realloc(g_HeapClasses[i]->m_object_context->m_class_instances, sizeof(heap_class_instance_t) * (g_HeapClasses[i]->m_object_context->m_class_instances_count - 1));
			return 0;
		} else if (g_HeapClasses[i]->m_object_context->m_class_instances_count == 1) {
			free(g_HeapClasses[i]->m_object_context->m_class_instances);
			return 0;
		} else {
			return 1;
		}
	}
	return 1;
}

int Heap_Destroy(void)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		//free(g_HeapClasses[i]);
	}
	return 0;
}