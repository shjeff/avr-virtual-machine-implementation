void ExecutionEngine_InstructionInfo(u1* pgm_code_ptr)
{
	dbg("Executing instruction: 0x%02X [", pgm_read_byte(pgm_code_ptr));
}

void ExecutionEngine_InstructionDebug(const char* instructionDesc)
{
	dbg("%s]\n", instructionDesc);
}

void ExecutionEngine_OPSDump(frame_t* frame)
{
	if (frame->m_operand_stack->m_size > MAX_OPERAND_STACK) {
		msg("\tStack size is too large!\n");
	} else {
		dbg("\tm_operand_stack->m_size: %d\n", frame->m_operand_stack->m_size);
		for (int i = 0; i < frame->m_operand_stack->m_size; i++) {
			if (frame->m_operand_stack->m_operands[i].m_heap_object_id_ptr)
				dbg("\tOPS[%d]: 0x%04X\n", i, (unsigned int)frame->m_operand_stack->m_operands[i].m_heap_object_id_ptr);
			else
				dbg("\tOPS[%d]: %lu\n", i, frame->m_operand_stack->m_operands[i].m_value.v_common);
		}
	}
	msg("----------------------------\n");
}

void ExecutionEngine_LVTDump(frame_t* frame)
{
	for (int i = 0; i < MAX_LOCAL_VARIABLE_TABLE; i++)
	{
		if (frame->m_local_variable_table->m_local_variables[i].m_heap_object_id_ptr)
			dbg("\tLVT[%d]: 0x%04X\n", i, (unsigned int)frame->m_local_variable_table->m_local_variables[i].m_heap_object_id_ptr);
		else
			dbg("\tLVT[%d]: %lu\n", i, frame->m_local_variable_table->m_local_variables[i].m_value.v_common);
	}
	msg("----------------------------\n");
}

void ExecutionEngine_MethodDump(method_t* method)
{
	dbg("\tmethod->m_signature: 0x%04X\n", method->m_signature);
	dbg("\tmethod->m_parameter_count: %d\n", method->m_parameter_count);
	dbg("\tmethod->m_code_length: %lu\n", method->m_code_length);
	dbg("\tmethod->m_class_index: %d\n", method->m_class_index);
	msg("----------------------------\n");
}

int ExecutionEngine_ExecuteFrame(void)
{
	frame_t* frame = FrameStack_PeekTopFrame();

	dbg("\nExecuting frame, class index: %d, method: 0x%04X\n", frame->m_method->m_class_index, frame->m_method->m_signature);
	int instructionJump = 0;
	do
	{
		instructionJump = 1;
		ExecutionEngine_InstructionInfo(frame->m_current_instruction_ptr);
		switch (pgm_read_byte(frame->m_current_instruction_ptr))
		{
		case OP_BIPUSH:
		{
			ExecutionEngine_InstructionDebug("OP_BIPUSH");
			s1 sByte = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			variable_t operand = {};
			operand.m_value.v_byte = sByte;
			OperandStack_PushOperand(frame->m_operand_stack, &operand);
			instructionJump = 2;
			break;
		}

		case OP_GOTO:
		{
			ExecutionEngine_InstructionDebug("OP_GOTO");
			u1 sJumpH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			u1 sJumpL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
			instructionJump = (s2)(sJumpH << 8 | sJumpL);
			break;
		}

		case OP_ICONST_0:
		case OP_ICONST_1:
		case OP_ICONST_2:
		case OP_ICONST_3:
		case OP_ICONST_4:
		case OP_ICONST_5:
		{
			ExecutionEngine_InstructionDebug("OP_ICONST");
			ExecutionEngine_OPSDump(frame);
			u1 uConst = pgm_read_byte(frame->m_current_instruction_ptr) - OP_ICONST_0;
			variable_t operand = {};
			operand.m_value.v_byte = uConst;
			OperandStack_PushOperand(frame->m_operand_stack, &operand);;
			ExecutionEngine_OPSDump(frame);
			break;
		}

		case OP_INVOKESTATIC:
		{
			ExecutionEngine_InstructionDebug("OP_INVOKESTATIC");
			
			instructionJump = 3;
			u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
			u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);
			
			u1 const_pool[5] = { 0, 0, 0, 0, 0 };
			
			ClassLoader_GetConstPoolAtIndex(frame->m_method->m_class_index, cp_index, const_pool, 5);
			
			signature_t class_signature = const_pool[1] << 8 | const_pool[2];
			signature_t method_signature = const_pool[3] << 8 | const_pool[4];
			if (((method_signature & METHOD_SIGNATURE_IS_INIT) != METHOD_SIGNATURE_IS_INIT) && (method_signature & METHOD_SIGNATURE_IS_NATIVE))
			{
				method_signature &= ~METHOD_SIGNATURE_IS_NATIVE;
				ExecutionEngine_ExecuteNativeMethod(class_signature, method_signature);
			}
			else
			{
				method_t callMethodInfo = {};
				ClassLoader_GetMethod(&callMethodInfo, class_signature, method_signature);
				int methodParameterCount = callMethodInfo.m_parameter_count;
				variable_t* methodArgs = 0;
				if (methodParameterCount)
					methodArgs = malloc(sizeof(variable_t) * methodParameterCount);
				for (int i = 0; i < methodParameterCount; i++)
					OperandStack_PopOperand(frame->m_operand_stack, &methodArgs[i]);
				ExecutionEngine_ExecuteStaticMethod(methodArgs, methodParameterCount, class_signature, method_signature);
			}
			break;
		}

		case OP_GETSTATIC:
		{
			ExecutionEngine_InstructionDebug("OP_GETSTATIC");
			instructionJump = 3;
			u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
			u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);

			u1 const_pool[5] = { 0, 0, 0, 0, 0 };

			ClassLoader_GetConstPoolAtIndex(frame->m_method->m_class_index, cp_index, const_pool, 5);

			signature_t class_signature = const_pool[1] << 8 | const_pool[2];
			signature_t field_signature = const_pool[3] << 8 | const_pool[4];

			variable_t getValue = Heap_GetStaticFieldValue(class_signature, field_signature);
			OperandStack_PushOperand(frame->m_operand_stack, &getValue);
			break;
		}

		case OP_PUTSTATIC:
		{
			ExecutionEngine_InstructionDebug("OP_PUTSTATIC");
			instructionJump = 3;
			u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
			u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);

			u1 const_pool[5] = { 0, 0, 0, 0, 0 };

			ClassLoader_GetConstPoolAtIndex(frame->m_method->m_class_index, cp_index, const_pool, 5);

			signature_t class_signature = const_pool[1] << 8 | const_pool[2];
			signature_t field_signature = const_pool[3] << 8 | const_pool[4];

			variable_t putValue = {};
			OperandStack_PopOperand(frame->m_operand_stack, &putValue);
			Heap_SetStaticFieldValue(class_signature, field_signature, putValue);
			break;
		}

		case OP_SIPUSH:
		{
			ExecutionEngine_InstructionDebug("OP_SIPUSH");
			u1 uByteH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
			u1 uByteL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
			variable_t operand = {};
			operand.m_value.v_short = uByteH << 8 | uByteL;
			OperandStack_PushOperand(frame->m_operand_stack, &operand);
			instructionJump = 3;
			break;
		}

		case OP_ISTORE_0:
		case OP_ISTORE_1:
		{
			ExecutionEngine_InstructionDebug("OP_ISTORE");
			int sIndex = pgm_read_byte(frame->m_current_instruction_ptr) - OP_ISTORE_0;
			variable_t var = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var);
			LocalVariableTable_SetVariable(frame->m_local_variable_table, &var, sIndex);
			break;
		}

		case OP_ILOAD_0:
		{
			ExecutionEngine_InstructionDebug("OP_ILOAD");
			variable_t var = {};
			LocalVariableTable_GetVariable(frame->m_local_variable_table, &var, 0);
			OperandStack_PushOperand(frame->m_operand_stack, &var);
			break;
		}

		case OP_IRETURN:
		case OP_RETURN:
		{
			ExecutionEngine_InstructionDebug("OP_#RETURN");
			break;
			variable_t* return_value_ptr = 0;

			if (pgm_read_byte(frame->m_current_instruction_ptr) != OP_RETURN)
			{
				variable_t return_value;
				OperandStack_PopOperand(frame->m_operand_stack, &return_value);
				return_value_ptr = &return_value;
			}

			ExecutionEngine_MethodReturn(frame, return_value_ptr);
			return 0;
		}

		case OP_IADD:
		{
			ExecutionEngine_InstructionDebug("OP_IADD");
			variable_t var0, var1, res = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var0);
			OperandStack_PopOperand(frame->m_operand_stack, &var1);
			res.m_value.v_byte = var0.m_value.v_byte + var1.m_value.v_byte;
			OperandStack_PushOperand(frame->m_operand_stack, &res);
			break;
		}

		case OP_IAND:
		{
			ExecutionEngine_InstructionDebug("OP_IAND");
			variable_t var0, var1, res = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var0);
			OperandStack_PopOperand(frame->m_operand_stack, &var1);
			res.m_value.v_common = var0.m_value.v_common & var0.m_value.v_common;
			OperandStack_PushOperand(frame->m_operand_stack, &res);
			break;
		}

		case OP_IFEQ:
		{
			ExecutionEngine_InstructionDebug("OP_IFEQ");
			variable_t var = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var);
			if (!var.m_value.v_common) {
				u1 sJumpH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
				u1 sJumpL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
				instructionJump = (s2)(sJumpH << 8 | sJumpL);
			}
			break;
		}

		case OP_ISHR:
		{
			ExecutionEngine_InstructionDebug("OP_IFEQ");
			variable_t var0, var1, res = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var1);
			OperandStack_PopOperand(frame->m_operand_stack, &var0);
			res.m_value.v_common = var0.m_value.v_common >> var1.m_value.v_common;
			OperandStack_PushOperand(frame->m_operand_stack, &res);
			break;
		}

		case OP_I2B:
		{
			variable_t var = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var);
			var.m_value.v_byte = (s1)var.m_value.v_common;
			OperandStack_PushOperand(frame->m_operand_stack, &var);
			break;
		}

		case OP_I2S:
		{
			variable_t var = {};
			OperandStack_PopOperand(frame->m_operand_stack, &var);
			var.m_value.v_short = (s2)var.m_value.v_common;
			OperandStack_PushOperand(frame->m_operand_stack, &var);
			break;
		}

		//case OP_NEW:
		//{
		//	ExecutionEngine_InstructionDebug("OP_NEW");
		//	instructionJump = 3;
		//	u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
		//	u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
		//	u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);

		//	u1 const_pool[3] = { 0, 0, 0 };

		//	ClassLoader_GetConstPoolAtIndex(frame->m_method->m_class_index, cp_index, const_pool, 3);
		//	signature_t class_signature = const_pool[1] << 8 | const_pool[2];
		//	variable_t reference = {};
		//	reference.m_heap_object_id_ptr = Heap_CreateClassInstance(class_signature);
		//	OperandStack_PushOperand(frame->m_operand_stack, &reference);
		//	break;
		//}

		case OP_DUP:
		{
			ExecutionEngine_InstructionDebug("OP_DUP");
			int stackSize = OperandStack_GetStackSize(frame->m_operand_stack);
			variable_t topOperand = frame->m_operand_stack->m_operands[stackSize - 1];
			OperandStack_PushOperand(frame->m_operand_stack, &topOperand);
			break;
		}

		//case OP_INVOKESPECIAL:
		//{
		//	ExecutionEngine_InstructionDebug("OP_INVOKESPECIAL");
		//	instructionJump = 3;
		//	u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
		//	u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
		//	u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);
		//	u1 const_pool[5] = { 0, 0, 0, 0, 0 };

		//	ClassLoader_GetConstPoolAtIndex(frame->m_method->m_class_index, cp_index, const_pool, 5);
		//	signature_t class_signature = const_pool[1] << 8 | const_pool[2];
		//	signature_t method_signature = const_pool[3] << 8 | const_pool[4];

		//	method_t invokeMethod = {};
		//	ClassLoader_GetMethod(&invokeMethod, class_signature, method_signature);
		//	int methodParameterCount = invokeMethod.m_parameter_count;
		//	variable_t* methodArgs = 0;

		//	if (methodParameterCount)
		//		methodArgs = malloc(sizeof(variable_t) * methodParameterCount);
		//	for (int i = 0; i < methodParameterCount; i++)
		//		OperandStack_PopOperand(frame->m_operand_stack, &methodArgs[i]);
		//	variable_t instanceReference = {};
		//	OperandStack_PopOperand(frame->m_operand_stack, &instanceReference);
		//	ExecutionEngine_ExecuteObjectInitializer(methodArgs, methodParameterCount, class_signature, method_signature, instanceReference);
		//	break;
		//}

		//case OP_ALOAD_0:
		//{
		//	ExecutionEngine_InstructionDebug("OP_ALOAD");
		//	variable_t var = {};
		//	LocalVariableTable_GetVariable(frame->m_local_variable_table, &var, 0);
		//	OperandStack_PushOperand(frame->m_operand_stack, &var);
		//	break;
		//}

		//case OP_ASTORE_0:
		//{
		//	ExecutionEngine_InstructionDebug("OP_ASTORE");
		//	variable_t var = {};
		//	OperandStack_PopOperand(frame->m_operand_stack, &var);
		//	LocalVariableTable_SetVariable(frame->m_local_variable_table, &var, 0);
		//	break;
		//}

		//case OP_PUTFIELD:
		//{
		//	ExecutionEngine_InstructionDebug("OP_PUTFIELD");
		//	u1 uConstPoolIndexH = pgm_read_byte(frame->m_current_instruction_ptr + 1);
		//	u1 uConstPoolIndexL = pgm_read_byte(frame->m_current_instruction_ptr + 2);
		//	u2 cp_index = (u2)(uConstPoolIndexH << 8 | uConstPoolIndexL);
		//	u1 const_pool[5] = { 0, 0, 0, 0, 0 };
		//	return 0;
		//	break;
		//}

		//case OP_GETFIELD:
		//{
		//	ExecutionEngine_InstructionDebug("OP_GETFIELD");
		//	break;
		//}

		default:
			break;
		}

		frame->m_program_counter += instructionJump;
		frame->m_current_instruction_ptr += instructionJump;
	} while (frame->m_program_counter < frame->m_method->m_code_length);

	return 0;
}

int ExecutionEngine_ExecuteObjectInitializer(variable_t* args, int argc, signature_t class_signature, signature_t method_signature, variable_t heap_instance)
{
	method_t initializerMethod = {};
	if (RESULT_SUCCESS(ClassLoader_GetObjectInitializerMethod(class_signature, method_signature, &initializerMethod)))
	{
		/// ExecutionEngine_InvokeMethod {
		FrameStack_CreateFrame();
		FrameStack_FillFrame(&initializerMethod);

		frame_t* top_frame = FrameStack_PeekTopFrame();
		LocalVariableTable_SetVariable(top_frame->m_local_variable_table, &heap_instance, 0);

		for (int i = 0; i < argc; i++)
			LocalVariableTable_SetVariable(top_frame->m_local_variable_table, &args[i], i + 1);

		if (RESULT_ERROR(ExecutionEngine_ExecuteFrame()))
			return 1;
		/// ExecutionEngine_InvokeMethod }
	}
	return 0;
}

int ExecutionEngine_InvokeMethod(variable_t* args, int argc, method_t* method)
{
	FrameStack_CreateFrame();
	FrameStack_FillFrame(method);

	frame_t* top_frame = FrameStack_PeekTopFrame();

	for (int i = 0; i < argc; i++)
		LocalVariableTable_SetVariable(top_frame->m_local_variable_table, &args[i], i);

	if (RESULT_ERROR(ExecutionEngine_ExecuteFrame()))
		return 1;

	return 0;
}

int ExecutionEngine_ExecuteStaticMethod(variable_t* args, int argc, signature_t class_signature, signature_t method_signature)
{
	method_t staticMethod = {};
	if (RESULT_SUCCESS(ClassLoader_GetMethod(&staticMethod, class_signature, method_signature)))
	{
		ExecutionEngine_InvokeMethod(args, argc, &staticMethod);
		return 0;
	}
	return 1;
}

int ExecutionEngine_MethodReturn(frame_t* current_frame, variable_t* return_value)
{
	// Free after method exit

	// Deallocate all istances if target method created class instances

	// Destroy top frame
	FrameStack_DestroyFrame();

	// Pass return value to lower frame's operand stack
	if (return_value) {
		frame_t* lower_frame = FrameStack_PeekTopFrame();
		OperandStack_PushOperand(lower_frame->m_operand_stack, return_value);
	}

	return 0;
}

int ExecutionEngine_ExecuteNativeMethod(signature_t native_class_signature, signature_t native_method_signature)
{
	frame_t* top_frame = FrameStack_PeekTopFrame();

	if (native_class_signature == NATIVE_CLASS_REGISTRY_SIGNATURE)
	{
		if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_REGISTRY_SETBIT_SIGNATURE)
		{
			variable_t arg0, arg1;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg1);
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NRegistry_setBit(arg0.m_value.v_byte, arg1.m_value.v_u3b);
		}
		else if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_REGISTRY_CLEARBIT_SIGNATURE)
		{
			variable_t arg0, arg1;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg1);
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NRegistry_clearBit(arg0.m_value.v_byte, arg1.m_value.v_u3b);
		}
		else if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_REGISTRY_TOGGLEBIT_SIGNATURE)
		{
			variable_t arg0, arg1;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg1);
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NRegistry_toggleBit(arg0.m_value.v_byte, arg1.m_value.v_u3b);
		}
		else if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_REGISTRY_SET_SIGNATURE)
		{
			variable_t arg0, arg1;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg1);
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NRegistry_set(arg0.m_value.v_byte, arg1.m_value.v_byte);
		}
	}
	else if (native_class_signature == NATIVE_CLASS_DELAY_SIGNATURE)
	{
		if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_DELAY_MSEC)
		{
			variable_t arg0;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NDelay_msec(arg0.m_value.v_short);
		}
		else if (native_method_signature == METHOD_SIGNATURE_IS_NATIVE_DELAY_SEC)
		{
			variable_t arg0;
			OperandStack_PopOperand(top_frame->m_operand_stack, &arg0);
			NDelay_msec(arg0.m_value.v_short);
		}
	}

	return 0;
}