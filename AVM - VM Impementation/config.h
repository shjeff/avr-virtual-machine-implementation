#ifndef __HEADER_CONFIG
#define __HEADER_CONFIG

/* Configuration (VM constants) */
#define ON			1
#define OFF			0

/* Debug USART character size */
#define DEBUG_USART_CFG_CHARACTER_SIZE_5_BIT		0x00
#define DEBUG_USART_CFG_CHARACTER_SIZE_6_BIT		0x01
#define DEBUG_USART_CFG_CHARACTER_SIZE_7_BIT		0x02
#define DEBUG_USART_CFG_CHARACTER_SIZE_8_BIT		0x03
#define DEBUG_USART_CFG_CHARACTER_SIZE_9_BIT		0x07

/* Debug USART mode */
#define DEBUG_USART_CFG_MODE_ASYNC					0x00
#define DEBUG_USART_CFG_MODE_SYNC					0x01

/* Debug USART parity mode */
#define DEBUG_USART_CFG_PARITY_MODE_DISABLED		0x00
#define DEBUG_USART_CFG_PARITY_MODE_EVEN_PARITY		0x02
#define DEBUG_USART_CFG_PARITY_MODE_ODD_PARITY		0x03

/* Debug USART stop bits */
#define DEBUG_USART_CFG_STOP_BITS_1					0x00
#define DEBUG_USART_CFG_STOP_BITS_2					0x01


/* User configuration */

/* VM Debugging */
#define DEBUG_MODE					ON
#define DEBUG_USE_USART				ON
#define  DEBUG_USE_USART_COMMANDS	OFF
#define DEBUG_USE_LEDS				ON
#define  DEBUG_USE_ASSERTS			ON

/* Debug USART baud rate 
 * Current USART configuration: 9600, 8N1N */
#define DEBUG_USART_CFG_BAUD_RATE					9600

#define DEBUG_USART_CFG_CHARACTER_SIZE				DEBUG_USART_CFG_CHARACTER_SIZE_8_BIT
#define DEBUG_USART_CFG_MODE						DEBUG_USART_CFG_MODE_ASYNC
#define DEBUG_USART_CFG_PARITY_MODE					DEBUG_USART_CFG_PARITY_MODE_DISABLED
#define DEBUG_USART_CFG_STOP_BITS					DEBUG_USART_CFG_STOP_BITS_1

#define MAX_FRAME_STACK				5
#define MAX_OPERAND_STACK			5
#define MAX_LOCAL_VARIABLE_TABLE	5
#define MAX_HEAP					1

/* Allocate static fields of class during initialization */
#define VM_CONFIG_USE_CLASS_STATIC_FIELDS_ALLOCATION		ON

/* Allocate static fields of class that are objects during initialization (depends on config above) */
#define  VM_CONFIG_USE_CLASS_STATIC_OBJECTS_ALLOCATION		OFF



#endif