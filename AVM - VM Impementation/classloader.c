int ClassLoader_GetEntryPointMethod(method_t* entry_point_method)
{
	for (int i = 0; i < TOTAL_CLASSES; ++i)
	{
		pgm_address_ptr pgm_addr = &ClassesMethods[i][0];

		u2 method_count = 0;
		be_pgm_read_u2_m(method_count, pgm_addr);
		for (int k = 0; k < method_count; k++)
		{
			u2 method_signature = 0;
			be_pgm_read_u2_m(method_signature, pgm_addr);

			u2 parameter_count = 0;
			be_pgm_read_u2_m(parameter_count, pgm_addr);

			u4 method_code_length = 0;
			be_pgm_read_u4_m(method_code_length, pgm_addr);

			if (((method_signature & METHOD_SIGNATURE_IS_INIT) != METHOD_SIGNATURE_IS_INIT) && (method_signature & METHOD_SIGNATURE_IS_ENTRY_POINT))
			{
				entry_point_method->m_code = pgm_addr;
				entry_point_method->m_code_length = method_code_length;
				entry_point_method->m_class_index = i;
				entry_point_method->m_parameter_count = parameter_count;
				entry_point_method->m_signature = method_signature;
				return 0;
			}
			else {
				mov_ptr(pgm_addr, method_code_length);
			}
		}
	}
	return 1;
}

int ClassLoader_GetStaticInitializerMethod(signature_t class_signature, method_t * initializer_method)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		u2 pgm_class_signature = be_pgm_read_u2(&ClassesSignatures[i][0]);

		if (pgm_class_signature != class_signature)
			continue;

		pgm_address_ptr pgm_addr = &ClassesMethods[i][0];
		u2 method_count = 0;
		be_pgm_read_u2_m(method_count, pgm_addr);

		for (int j = 0; j < method_count; j++)
		{
			u2 pgm_method_signature = 0;
			be_pgm_read_u2_m(pgm_method_signature, pgm_addr);

			u2 parameter_count = 0;
			be_pgm_read_u2_m(parameter_count, pgm_addr);

			u4 code_length = 0;
			be_pgm_read_u4_m(code_length, pgm_addr);

			if ((pgm_method_signature & METHOD_SIGNATURE_IS_INIT) == METHOD_SIGNATURE_IS_INIT)
			{
				if (METHOD_SIGNATURE_GET_CONTEXT(pgm_method_signature) == METHOD_SIGNATURE_STATIC_INIT)
				{
					initializer_method->m_class_index = i;
					initializer_method->m_signature = pgm_method_signature;
					initializer_method->m_code_length = code_length;
					initializer_method->m_code = pgm_addr;
					initializer_method->m_parameter_count = parameter_count;
					return 0;
				}
			}
			mov_ptr(pgm_addr, code_length);
		}
	}

	return 1;
}

int ClassLoader_GetObjectInitializerMethod(signature_t class_signature, signature_t constructor_method_signature, method_t * initializer_method)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		u2 pgm_class_signature = be_pgm_read_u2(&ClassesSignatures[i][0]);

		if (pgm_class_signature != class_signature)
			continue;

		pgm_address_ptr pgm_addr = &ClassesMethods[i][0];
		u2 method_count = 0;
		be_pgm_read_u2_m(method_count, pgm_addr);

		for (int j = 0; j < method_count; j++)
		{
			u2 pgm_method_signature = 0;
			be_pgm_read_u2_m(pgm_method_signature, pgm_addr);

			u2 parameter_count = 0;
			be_pgm_read_u2_m(parameter_count, pgm_addr);

			u4 code_length = 0;
			be_pgm_read_u4_m(code_length, pgm_addr);

			if ((pgm_method_signature & METHOD_SIGNATURE_IS_INIT) == METHOD_SIGNATURE_IS_INIT)
			{
				if (METHOD_SIGNATURE_GET_CONTEXT(pgm_method_signature) == METHOD_SIGNATURE_OBJECT_INIT)
				{
					if (pgm_method_signature == constructor_method_signature)
					{
						initializer_method->m_class_index = i;
						initializer_method->m_signature = pgm_method_signature;
						initializer_method->m_code_length = code_length;
						initializer_method->m_code = pgm_addr;
						initializer_method->m_parameter_count = parameter_count;
						return 0;
					}
				}
			}
			mov_ptr(pgm_addr, code_length);
		}
	}

	return 1;
}

int ClassLoader_GetMethod(method_t* method, signature_t class_signature, signature_t method_signature)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		pgm_address_ptr pgm_addr = 0;
		pgm_addr = &ClassesSignatures[i][0];

		u2 pgm_class_signature = be_pgm_read_u2(pgm_addr);

		if (pgm_class_signature != class_signature)
			continue;

		pgm_addr = &ClassesMethods[i][0];

		u2 method_count = be_pgm_read_u2(pgm_addr);
		mov_ptr(pgm_addr, 2);

		for (int j = 0; j < method_count; j++)
		{
			u2 pgm_method_signature = be_pgm_read_u2(pgm_addr);
			mov_ptr(pgm_addr, 2);

			u2 parameter_count = be_pgm_read_u2(pgm_addr);
			mov_ptr(pgm_addr, 2);

			u4 code_length = be_pgm_read_u4(pgm_addr);
			mov_ptr(pgm_addr, 4);

			if (pgm_method_signature != method_signature) {
				mov_ptr(pgm_addr, code_length);
				continue;
			}

			method->m_signature = method_signature;
			method->m_parameter_count = parameter_count;
			method->m_code_length = code_length;
			method->m_code = pgm_addr;
			method->m_class_index = i;
			return 0;
		}
	}

	return 1;
}

int ClassLoader_GetConstPoolSize(pgm_address_ptr pUtf8ConstPoolStartPosition)
{
	u1 tag = pgm_read_byte(pUtf8ConstPoolStartPosition);

	if (tag == CONST_POOL_ID_Utf8)
	{
		return 3 + be_pgm_read_u2(1 + pUtf8ConstPoolStartPosition);
	}
	else
	{
		if (tag & CONST_POOL_ID_SkipPoolFlag)
			return 1;

		switch (tag)
		{
		case CONST_POOL_ID_Float:
		case CONST_POOL_ID_Integer:
		case CONST_POOL_ID_Methodref:
		case CONST_POOL_ID_Fieldref:
		case CONST_POOL_ID_InterfaceMethodref:
		case CONST_POOL_ID_NameAndType:
			return 5;
		case CONST_POOL_ID_Class:
		case CONST_POOL_ID_String:
			return 3;
		case CONST_POOL_ID_Long:
		case CONST_POOL_ID_Double:
			return 9;
		}
		return 0;
	}
}

int ClassLoader_GetConstPoolAtIndex(int class_index, u2 const_pool_index, u1* const_pool, int const_pool_size)
{
	pgm_address_ptr pgm_addr = &ClassesConstPools[class_index][0];

	u2 const_pool_count = 0;
	be_pgm_read_u2_m(const_pool_count, pgm_addr);

	for (int i = 0; i < const_pool_count; i++)
	{
		int current_cp_size = ClassLoader_GetConstPoolSize(pgm_addr);

		if ((i + 1) == const_pool_index)
		{
			for (int k = 0; k < const_pool_size; k++)
				const_pool[k] = (u1)pgm_read_byte(pgm_addr++);
			return 0;
		}
		else {
			mov_ptr(pgm_addr, current_cp_size);
		}
	}

	return 1;
}
