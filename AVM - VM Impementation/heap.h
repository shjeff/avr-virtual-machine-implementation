#ifndef __HEADER_HEAP
#define __HEADER_HEAP

extern heap_class_t* g_HeapClasses[TOTAL_CLASSES];

int Heap_InitClassesStaticContext(void);
int Heap_Destroy(void);
variable_t* Heap_GetStaticFieldAddress(signature_t class_signature, signature_t field_signature);
int Heap_SetStaticFieldValue(signature_t class_signature, signature_t field_signature, variable_t value);
variable_t Heap_GetStaticFieldValue(signature_t class_signature, signature_t field_signature);
heap_class_instance_t* Heap_CreateClassInstance(signature_t class_signature);
int Heap_DestroyClassInstance(signature_t class_signature, heap_class_instance_t* heap_class_instance);


#include "heap.c"

#endif