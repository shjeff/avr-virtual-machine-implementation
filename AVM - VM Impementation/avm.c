#ifndef __AVR_ATmega32__
#define __AVR_ATmega32__
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

/* AVR Virtual Machine Configuration */
#include "config.h"

/* Basic types */
typedef uint8_t		u1;
typedef uint16_t	u2;
typedef uint32_t	u4;

typedef  int8_t		s1;
typedef  int16_t	s2;
typedef  int32_t	s4;

/* Class info file */
#include "classes/class_info.h"

/* Types define */
#include "types.h"

/* Debugging */
#include "debug.h"

/* Defines */
#include "defines.h"

/* OP codes */
#include "opcodes.h"

/* Native */
#include "native.h"

#include "ops.h"
#include "lvt.h"

#define DEBUG_USE_DUMPS ON

#if DEBUG_USE_DUMPS	== OFF
static inline void dump_ops(frame_t* current_frame) { }
static inline void dump_lvt(frame_t* current_frame) { }
static inline void dump_frm(frame_t* current_frame) { }
static inline void dump_fnc(method_t* method) { }
#else
static inline void dump_fnc(method_t* method)
{
	msg("\t// === FUNC dump === ///\n");
	dbg("\tmethod->m_signature: 0x%04X\n", method->m_signature);
	dbg("\tmethod->m_parameter_count: %d\n", method->m_parameter_count);
	dbg("\tmethod->m_code: %p\n", method->m_code);
	dbg("\tmethod->m_code_length: %lu\n", method->m_code_length);
	dbg("\tmethod->m_class_index: %d\n", method->m_class_index);
	msg("\t// ================= ///\n");
}

static inline void dump_ops(frame_t* current_frame)
{
	msg("\t// === OPS dump === ///\n");

	dbg("\toperand_stack->m_size: %d\n", current_frame->m_operand_stack->m_size);
	int to = current_frame->m_operand_stack->m_size > MAX_OPERAND_STACK ? MAX_OPERAND_STACK : current_frame->m_operand_stack->m_size;
	for (int i = 0; i < to; i++)
	{
		if (current_frame->m_operand_stack->m_operands[i].m_heap_object_id_ptr) {
			dbg("\top[%d]: %p\n", i, current_frame->m_operand_stack->m_operands[i].m_heap_object_id_ptr);
		}
		else {
			dbg("\top[%d]: %d\n", i, current_frame->m_operand_stack->m_operands[i].m_value.v_byte);
		}
	}
	msg("\t// ================ ///\n");
}

static inline void dump_lvt(frame_t* current_frame)
{
	msg("\t// === LVT dump === ///\n");

	for (int i = 0; i < MAX_LOCAL_VARIABLE_TABLE; i++)
	{
		if (current_frame->m_local_variable_table->m_local_variables[i].m_heap_object_id_ptr) {
			dbg("\tlvt[%d]: %p\n", i, current_frame->m_local_variable_table->m_local_variables[i].m_heap_object_id_ptr);
		}
		else {
			dbg("\tlvt[%d]: %d\n", i, current_frame->m_local_variable_table->m_local_variables[i].m_value.v_byte);
		}
	}

	msg("\t// ================ ///\n");
}

static inline void dump_frm(frame_t* current_frame)
{
	msg("\t////[[ === FRM dump === ]]///\n");
	dbg("\tcurrent_frame->m_current_instruction_ptr: %p\n", current_frame->m_current_instruction_ptr);
	dbg("\tcurrent_frame->m_program_counter: %lu\n", current_frame->m_program_counter);
	dbg("\t\tcurrent_frame->m_method->m_class_index: %d\n", current_frame->m_method->m_class_index);
	dbg("\t\tcurrent_frame->m_method->m_signature: 0x%04X\n", current_frame->m_method->m_signature);
	dbg("\t\tcurrent_frame->m_method->m_parameter_count: %d\n", current_frame->m_method->m_parameter_count);
	dbg("\t\tcurrent_frame->m_method->m_code_length: %lu\n", current_frame->m_method->m_code_length);
	dbg("\t\tcurrent_frame->m_method->m_code: %p\n", current_frame->m_method->m_code);
	dump_ops(current_frame);
	dump_lvt(current_frame);
	msg("\t////[[ ================ ]]///\n");
}
#endif

#include "classloader.h"

#include "frame.h"
#include "heap.h"

#if DEBUG_USE_DUMPS == OFF
static inline void dump_heap(void) { }
#else
static inline void dump_heap(void)
{
	msg("\t// === HEAP dump === ///\n");
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		if (!g_HeapClasses[i])
			continue;

		dbg("\tHeap class (%d), sig: 0x%04X\n", i, g_HeapClasses[i]->m_class_signature);
		msg("\tStatic context:\n");
		for (int j = 0; j < g_HeapClasses[i]->m_static_context->m_static_fields_count; j++) {
			dbg("\tfield (0x%04X): %d\n", g_HeapClasses[i]->m_static_context->m_static_fields[j].m_signature,
				g_HeapClasses[i]->m_static_context->m_static_fields[j].m_field_value.m_value.v_byte);
		}

		msg("\tInstance context:\n");
		for (int j = 0; j < g_HeapClasses[i]->m_object_context->m_class_instances_count; j++)
		{
			dbg("\tClass instance: %p\n", &g_HeapClasses[i]->m_object_context->m_class_instances[i]);
			for (int k = 0; k < g_HeapClasses[i]->m_non_static_fields_count; k++)
			{
				heap_variable_t field = g_HeapClasses[i]->m_object_context->m_class_instances[j].m_instance_fields[k];
				dbg("\tinstance field, sig: 0x%04X, value: %lu\n", field.m_signature, field.m_field_value.m_value.v_common);
			}
		}
	}
	msg("\t// ================ ///\n");
}
#endif

#include "engine.h"

frame_t* g_FrameStack[MAX_FRAME_STACK];
int		 g_FrameStackCurrentSize = 0;

heap_class_t* g_HeapClasses[TOTAL_CLASSES];

/// =============================== VM global functions =============================== ///}

void VM_ExecuteStaticInitializers(void)
{
	for (int i = 0; i < TOTAL_CLASSES; i++)
	{
		method_t staticInitializerMethod = {};

		if (RESULT_SUCCESS(ClassLoader_GetStaticInitializerMethod(g_HeapClasses[i]->m_class_signature, &staticInitializerMethod)))
		{
			ExecutionEngine_InvokeMethod(0, 0, &staticInitializerMethod);
			return;
		}
	}
}

int main(void)
{
	led_blink_on_vm_start;

	//DDRC = 0xFF;
	//PORTC = 0;

	//for (;;) {
	//	PORTC ^= (1uL << 0);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 1);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 2);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 3);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 4);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 5);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 6);
	//	_delay_ms(200);

	//	PORTC ^= (1uL << 7);
	//	_delay_ms(200); _delay_ms(200); _delay_ms(200);
	//	_delay_ms(200); _delay_ms(200); _delay_ms(200);
	//	_delay_ms(200); _delay_ms(200); _delay_ms(200);
	//	_delay_ms(200); _delay_ms(200); _delay_ms(200);
	//}

	/* VM debugging */
	avm_dbg_init();
	
	cli();

	/* Entry point method */
	method_t entry_point_method = {};

	/* Create static context of all classes */
	//Heap_InitClassesStaticContext();

	/* Execute static initializer methods */
	//VM_ExecuteStaticInitializers();

	if (RESULT_SUCCESS(ClassLoader_GetEntryPointMethod(&entry_point_method)))
	{
		msg_info("Found an entry point method, signature: 0x%04X\n", entry_point_method.m_signature);
		if (RESULT_SUCCESS(ExecutionEngine_InvokeMethod(0, 0, &entry_point_method)))
		{
			//msg_info("VM program reached the end\n");
			__infinity_loop;
			return 0;
		}

		return 1;
	}

	//msg_err("Couldn't find an entry point method. Make sure you have this method included in some class\n");
	return 1;
}