#ifndef __HEADER_FRAME
#define __HEADER_FRAME

extern frame_t*		g_FrameStack[MAX_FRAME_STACK];
extern int			g_FrameStackCurrentSize;

int FrameStack_CreateFrame(void);
int FrameStack_DestroyFrame(void);
void FrameStack_FillFrame(method_t* method);
frame_t* FrameStack_PeekTopFrame(void);

#include "frame.c"

#endif