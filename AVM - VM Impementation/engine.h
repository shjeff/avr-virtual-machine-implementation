#ifndef __HEADER_ENGINE
#define __HEADER_ENGINE

int  ExecutionEngine_InvokeMethod(variable_t* args, int argc, method_t* method);
int  ExecutionEngine_ExecuteStaticMethod(variable_t* args, int argc, signature_t class_signature, signature_t method_signature);
int  ExecutionEngine_MethodReturn(frame_t* current_frame, variable_t* return_value);
void ExecutionEngine_InstructionInfo(u1* pgm_code_ptr);
int  ExecutionEngine_ExecuteFrame(void);
int  ExecutionEngine_ExecuteNativeMethod(signature_t native_class_signature, signature_t native_method_signature);
int  ExecutionEngine_ExecuteObjectInitializer(variable_t* args, int argc, signature_t class_signature, signature_t method_signature, variable_t heap_instance);
#include "engine.c"

#endif