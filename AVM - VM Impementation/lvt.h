#ifndef __HEADER_LOCAL_VARIABLE_TABLE
#define __HEADER_LOCAL_VARIABLE_TABLE



static int LocalVariableTable_SetVariable(local_variable_table_t* local_variable_table, variable_t* variable, int index)
{
	if (index >= MAX_LOCAL_VARIABLE_TABLE || index < 0)
		return 1;

	local_variable_table->m_local_variables[index] = *variable;
	return 0;
}

static int LocalVariableTable_GetVariable(local_variable_table_t* local_variable_table, variable_t* variable, int index)
{
	if (index >= MAX_LOCAL_VARIABLE_TABLE || index < 0)
		return 1;

	*variable = local_variable_table->m_local_variables[index];
	return 0;
}

#endif