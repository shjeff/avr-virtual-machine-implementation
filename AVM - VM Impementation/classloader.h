#ifndef __HEADER_CLASSLOADER
#define __HEADER_CLASSLOADER

int ClassLoader_GetEntryPointMethod(method_t* entry_point_method);
int ClassLoader_GetMethod(method_t* method, signature_t class_signature, signature_t method_signature);
int ClassLoader_GetConstPoolSize(pgm_address_ptr pUtf8ConstPoolStartPosition);
int ClassLoader_GetConstPoolAtIndex(int class_index, u2 const_pool_index, u1* const_pool, int const_pool_size);
int ClassLoader_GetStaticInitializerMethod(signature_t class_signature, method_t * initializer_method);
int ClassLoader_GetObjectInitializerMethod(signature_t class_signature, signature_t constructor_method_signature, method_t * initializer_method);


#include "classloader.c"

#endif