static inline uint8_t NRegistry_get(uint8_t address)
{
	return get_reg(address);
}

static inline void NRegistry_set(uint8_t address, uint8_t value)
{
	dbg("a: 0x%02X, v: 0x%02X\n", address, value);
	set_reg(address, value);
}

static inline void NRegistry_setBit(uint8_t address, u1 bit_index)
{
	set_reg(address, get_reg(address)|(1uL << bit_index));
}

static inline void NRegistry_clearBit(uint8_t address, u1 bit_index)
{
	set_reg(address, get_reg(address) & ~(1uL << bit_index));
}

static inline void NRegistry_setBitState(uint8_t address, u1 bit_index, u1 bit_state)
{
	// ...
}

static inline void NRegistry_toggleBit(uint8_t address, u1 bit_index)
{
	set_reg(address, get_reg(address) ^ (1uL << bit_index));
}

static inline u1 NRegistry_getBit(uint8_t address, u1 bit_index)
{
	return (get_reg(address) >> bit_index) & 1uL;
}


static inline void NDelay_msec(u2 time)
{
	_delay_ms(time);
}

static inline void NDelay_usec(u2 time)
{
	// microseconds??? on VM ??? IMPOSSIBLE!!!!
	//_delay_us(time);
}

static inline void NDelay_sec(u2 time)
{
	for (int i = 0; i < time * 4; ++i)
		NDelay_msec(250);
}

