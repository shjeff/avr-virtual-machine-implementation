#ifndef __HEADER_OPERAND_STACK
#define __HEADER_OPERAND_STACK



static int OperandStack_PushOperand(operand_stack_t* operand_stack, variable_t* operand)
{
	if (operand_stack->m_size < MAX_OPERAND_STACK)
	{
		operand_stack->m_operands[operand_stack->m_size++] = *operand;
		return 0;
	}
	return 1;
}

static int OperandStack_PopOperand(operand_stack_t* operand_stack, variable_t* operand)
{
	if (operand_stack->m_size)
	{
		*operand = operand_stack->m_operands[--operand_stack->m_size];
		return 0;
	}
	return 1;
}

static int OperandStack_GetStackSize(operand_stack_t* operand_stack)
{
	return operand_stack->m_size;
}

#endif