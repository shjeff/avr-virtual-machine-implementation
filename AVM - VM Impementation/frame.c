int FrameStack_CreateFrame(void)
{
	if (g_FrameStackCurrentSize < MAX_FRAME_STACK)
	{
		// Alokacja pami�ci nowej ramki, zwi�kszenie liczby zaalokowanych ramek na stosie
		g_FrameStack[g_FrameStackCurrentSize] = malloc(sizeof(frame_t));
		
		g_FrameStack[g_FrameStackCurrentSize]->m_local_variable_table = malloc(sizeof(local_variable_table_t));
		g_FrameStack[g_FrameStackCurrentSize]->m_operand_stack = malloc(sizeof(operand_stack_t));

		g_FrameStack[g_FrameStackCurrentSize]->m_operand_stack->m_size = 0;

		g_FrameStack[g_FrameStackCurrentSize]->m_program_counter = 0;
		g_FrameStack[g_FrameStackCurrentSize]->m_current_instruction_ptr = 0;
		g_FrameStackCurrentSize++;
		return 0;
	}
	return 1;
}

int FrameStack_DestroyFrame(void)
{
	if (g_FrameStackCurrentSize)
	{
		// Pobranie ramki, kt�ra jest na samej g�rze
		frame_t* top_frame = FrameStack_PeekTopFrame();

		// Dealkoacja tabeli lokalnych zmiennych i stosu operand�w
		free(top_frame->m_local_variable_table);
		free(top_frame->m_operand_stack);

		// Usuni�cie wska�nika do metody
		top_frame->m_method = 0;

		// Dealokacja ramki
		free(g_FrameStack[--g_FrameStackCurrentSize]);
		return 0;
	}
	return 1;
}

void FrameStack_FillFrame(method_t* method)
{
	// Pobranie ramki, kt�ra jest na samej g�rze
	frame_t* top_frame = FrameStack_PeekTopFrame();

	for (int i = 0; i < MAX_LOCAL_VARIABLE_TABLE; i++){
		top_frame->m_local_variable_table->m_local_variables[i].m_value.v_byte = 0;
		top_frame->m_local_variable_table->m_local_variables[i].m_heap_object_id_ptr = 0;
	}

	for (int i = 0; i < MAX_OPERAND_STACK; i++)
		top_frame->m_operand_stack->m_operands[i].m_value.v_byte = 0;
	
	// Inicjalizacja wska�nika bie��cej instrukcji
	top_frame->m_current_instruction_ptr = method->m_code;
	
	// Ustawienie wska�nika do metody
	top_frame->m_method = method;
}

frame_t* FrameStack_PeekTopFrame(void)
{
	if (!g_FrameStackCurrentSize)
		return 0;

	return g_FrameStack[g_FrameStackCurrentSize - 1];
}