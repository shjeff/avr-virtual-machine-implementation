#ifndef __HEADER_DEBUG
#define __HEADER_DEBUG

#if DEBUG_MODE == ON

/* Debug by UART */
#if DEBUG_USE_USART == ON

/* Include stdio library for printf function */
#include <stdio.h>

/* Calculate UBRR register value */
#define USART_UBRR (F_CPU/16/DEBUG_USART_CFG_BAUD_RATE-1)

static int __internal__uart_OutputPutCharacter(char character, FILE *stream)
{
	if (character == '\n')
		__internal__uart_OutputPutCharacter('\r', stream);
	loop_until_bit_is_set(UCSRA, UDRE);
	UDR = character;
	return 0;
}

#if DEBUG_USE_USART_COMMANDS == ON
#define		uart_put_byte(character) \
	loop_until_bit_is_set(UCSRA, UDRE); \
	UDR = character;
#else
#define		uart_put_byte(character)
#endif

static FILE UART_StdOut = FDEV_SETUP_STREAM(__internal__uart_OutputPutCharacter, NULL, _FDEV_SETUP_WRITE);

#ifdef __AVR_ATmega32__
static inline void dbg_usart_init(void)
{
	u1 UCSRCReg = 0;
	UBRRH = USART_UBRR >> 8;
	UBRRL = USART_UBRR & 0xFF;

	UCSRB = (1uL << TXEN) | ((DEBUG_USART_CFG_CHARACTER_SIZE >> 2) << 2);

	UCSRCReg = (DEBUG_USART_CFG_CHARACTER_SIZE & 0x03) << 1;
	UCSRCReg |= DEBUG_USART_CFG_STOP_BITS << 3;
	UCSRCReg |= DEBUG_USART_CFG_PARITY_MODE << 4;
	UCSRCReg |= DEBUG_USART_CFG_PARITY_MODE << 4;
	UCSRCReg |= DEBUG_USART_CFG_MODE << 6;

	UCSRCReg |= (1uL << URSEL);

	UCSRC = UCSRCReg;

	stdout = &UART_StdOut;
}

#define msg(m)				printf(m)
#define msg_err(m, ...)		printf("[ERROR] " m, ## __VA_ARGS__)
#define msg_info(m, ...)	printf("[INFO] " m, ## __VA_ARGS__)
#define msg_warn(m, ...)	printf("[WARNING] " m, ## __VA_ARGS__)

#define dbg_info(m, ...)	printf("[%s:(%d)] " m, __func__, __LINE__, ## __VA_ARGS__)
#define dbg(m, ...)			printf(m, ## __VA_ARGS__)
#elif __AVR_ATmega8__
static inline void dbg_usart_init(void)
{
	u1 UCSRCReg = 0;
	UBRRH = USART_UBRR >> 8;
	UBRRL = USART_UBRR & 0xFF;

	UCSRB = (1uL << TXEN) | (DEBUG_USART_CFG_CHARACTER_SIZE >> 2);

	UCSRCReg = (DEBUG_USART_CFG_CHARACTER_SIZE & 0x03) << 1;
	UCSRCReg |= DEBUG_USART_CFG_STOP_BITS << 3;
	UCSRCReg |= DEBUG_USART_CFG_PARITY_MODE << 4;
	UCSRCReg |= DEBUG_USART_CFG_PARITY_MODE << 4;
	UCSRCReg |= DEBUG_USART_CFG_MODE << 6;

	UCSRCReg |= (1uL << URSEL);

	UCSRC = UCSRCReg;

	stdout = &UART_StdOut;
}

#define msg(m)				printf(m)
#define msg_err(m, ...)		printf("[ERROR] " m, ## __VA_ARGS__)
#define msg_info(m, ...)	printf("[INFO] " m, ## __VA_ARGS__)
#define msg_warn(m, ...)	printf("[WARNING] " m, ## __VA_ARGS__)

#define dbg_info(m, ...)	printf("[%s:(%d)] " m, __func__, __LINE__, ## __VA_ARGS__)
#define dbg(m, ...)			printf(m, ## __VA_ARGS__)
#endif

#endif


/* Debug by LEDs */
#if DEBUG_USE_LEDS == ON

#define LED_R		2
#define LED_G		3

static inline void dbg_led_init(void)
{
	DDRD |= (1uL << LED_R) | (1uL << LED_G);
	PORTD &= ~((1uL << LED_R) | (1uL << LED_G));
}

static inline void dbg_led_blink(int led)
{
	PORTD |= (1uL << led);
	_delay_ms(200);
	PORTD &= ~(1uL << led);
	_delay_ms(200);
}

static inline void dbg_led_tick_n(int led, int n)
{
	int i = 0;
	for (; i < n; ++i)
		dbg_led_blink(led);
}

static inline void dbg_led_toggle(int led)
{
	PORTD ^= (1uL << led);
}

static inline void dbg_led_on(int led)
{
	PORTD |= (1uL << led);
}

static inline void dbg_led_off(int led)
{
	PORTD &= ~(1uL << led);
}

/* Note, that VM start LED uses PC5 pin */
#define led_blink_on_vm_start \
	DDRD |= (1uL << 5); \
	PORTD |= (1uL << 5); \
		_delay_ms(255); \
	PORTD &= ~(1uL << 5); \
	_delay_ms(255);

#define lr			dbg_led_on(LED_R)
#define lg			dbg_led_on(LED_G)
#define tr			dbg_led_toggle(LED_R);_delay_ms(100)
#define tg			dbg_led_toggle(LED_G);_delay_ms(100)

#define lt \
	if (!((PORTD & (1uL << LED_R)) ^ (PORTD & (1uL << LED_G))))	\
		PORTD |= (1uL << LED_R); \
	dbg_led_toggle(LED_R); \
	dbg_led_toggle(LED_G); \
	_delay_ms(200);


#if DEBUG_USE_ASSERTS == ON
#define assert(condition) \
	if (condition) \
		lg;	\
	else \
		lr;
#endif


#endif // DEBUG_USE_LEDS == ON

#endif // DEBUG_MODE == ON

#if DEBUG_USE_USART == OFF || DEBUG_MODE == OFF
static int UART_PutByte(char character)
{ return 0; }

static inline void dbg_usart_init(void) { }

#define msg(m)
#define dbg(m, ...)
#define msg_err(m, ...)
#define msg_info(m, ...)
#define msg_warn(m, ...)
#endif

#if DEBUG_USE_LEDS == OFF || DEBUG_MODE == OFF
static inline void dbg_led_init(void) { }
static inline void dbg_led_blink(int led) { }
static inline void dbg_led_tick_n(int led, int n) { }
static inline void dbg_led_toggle(int led) { }
static inline void dbg_led_on(int led) { }
static inline void dbg_led_off(int led) { }

#define led_blink_on_vm_start

#define lr
#define lg
#define tr
#define tg

#define lt
#endif

#if DEBUG_USE_LEDS == OFF || DEBUG_MODE == OFF
#define assert(condition)
#endif

static inline void avm_dbg_init(void)
{
	if (DEBUG_USE_USART)
		dbg_usart_init();

	if (DEBUG_USE_LEDS)
		dbg_led_init();
}

#endif // ndef __HEADER_DEBUG
