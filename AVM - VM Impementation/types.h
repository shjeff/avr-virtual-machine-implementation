#ifndef __HEADER_TYPES
#define __HEADER_TYPES

#include "defines.h"

/* Variable type */
typedef u4 type_t;

/* Signature */
typedef u2 signature_t;

/* Program space memory byte address */
typedef u1* pgm_address_ptr;

/* Bytecode */
typedef u1* bytecode_ptr_t;

typedef enum {
	VARIABLE_TYPE_HEAP_PTR,
	VARIABLE_TYPE_BOOLEAN,
	VARIABLE_TYPE_U4B,
	VARIABLE_TYPE_U3B,
	VARIABLE_TYPE_U2B,
	VARIABLE_TYPE_S4B,
	VARIABLE_TYPE_S3B,
	VARIABLE_TYPE_S2B,
	VARIABLE_TYPE_BYTE,
	VARIABLE_TYPE_SHORT,
	VARIABLE_TYPE_INT,
	VARIABLE_TYPE_FLOAT,
	VARIABLE_TYPE_LONG,
	VARIABLE_TYPE_DOUBLE,
} variable_type_t;

/// Variable structure ///

typedef struct {
	variable_type_t  m_var_type;
	void* m_heap_object_id_ptr;
	union {
		u1 v_boolean : 1;
		u1 v_u4b : 4;
		u1 v_u3b : 3;
		u1 v_u2b : 2;
		s1 v_s4b : 4;
		s1 v_s3b : 3;
		s1 v_s2b : 2;
		s1 v_byte;
		s2 v_short;
		s4 v_int;
		s4 v_float;
		u4 v_w_long;
		u4 v_w_double;
		u4 v_common;
	} m_value;
} variable_t;



/// Method structure ///

typedef struct {
	signature_t 	m_signature;
	int				m_class_index;
	u2				m_parameter_count;
	u4  			m_code_length;
	bytecode_ptr_t	m_code;
} method_t;



/// Operand stack structure ///

typedef struct {
	variable_t		m_operands[MAX_OPERAND_STACK];
	volatile u2		m_size;
} operand_stack_t;



/// Local variable table structure ///

typedef struct {
	variable_t	m_local_variables[MAX_LOCAL_VARIABLE_TABLE];
} local_variable_table_t;



/// Frame structure ///

typedef struct {
	operand_stack_t*			m_operand_stack;
	local_variable_table_t*		m_local_variable_table;

	u4							m_program_counter;
	u1*							m_current_instruction_ptr;

	method_t*					m_method;
} frame_t;


/// Heap variable structure ///



typedef struct {
	type_t		m_type;
	variable_t	m_field_value;
	signature_t	m_signature;
} heap_variable_t;

typedef struct {
	heap_variable_t*		m_instance_fields;
	u2						m_instance_fields_count;
} heap_class_instance_t;

typedef struct {
	heap_class_instance_t*	m_class_instances;
	u2						m_class_instances_count;
} heap_object_context_t;

typedef struct {
	heap_variable_t*		m_static_fields;
	u2						m_static_fields_count;
} heap_static_context_t;

typedef heap_class_instance_t*	heap_object_ptr_t;


/// Heap class structure ///

typedef struct {
	signature_t				m_class_signature;

	heap_static_context_t*	m_static_context;
	heap_object_context_t*	m_object_context;

	u2						m_non_static_fields_count;

} heap_class_t;



/// Heap structure ///

typedef struct {
	heap_class_t*		m_classes[TOTAL_CLASSES];
} heap_t;



/* Little endian to big endian conversion for u4 */
static inline u4 le32_to_be(u4 le)
{
	u4 be = 0;
	for (int i = 0, j = 3; i < 4; i++, j--)
		be |= ((le >> (j << 3)) & 0xFF) << (i << 3);
	return be;
}

/* Little endian to big endian conversion for u2 */
static inline u2 le16_to_be(u2 le)
{
	u2 be = 0;
	for (int i = 0, j = 1; i < 2; i++, j--)
		be |= ((le >> (j << 3)) & 0xFF) << (i << 3);
	return be;
}

//#define pgm_read_byte(address_short)    pgm_read_byte_near(address_short)

#ifdef pgm_read_byte
#undef pgm_read_byte
#endif

static inline u1 pgm_read_byte(u1* address)
{
	_delay_ms(10);
	return pgm_read_byte_near(address);
}

#define pgm_read_byte(address_short)	pgm_read_byte(address_short)

#endif